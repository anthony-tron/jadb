import {Word} from './types';
import assert from 'assert';
import {read} from 'fs';

const lineRegex = /^([^\s]+)\s*(?:\[(.*)])?\s*\/(.*)\//;
const rightRegex = /(\(.*\))\s(.*)/;

export function parseLine(line: string): Word {
    const match = line.match(lineRegex);
    if (match == null)
        throw new Error('Not a word: ' + line);

    const [, writingSection, readingSection, rightSection] = match;
    assert(writingSection != undefined, line);
    assert(rightSection != undefined, line);

    const writings = writingSection.split(';');
    const readings = readingSection ? readingSection.split(';') : [];
    const rightParts = rightSection.match(rightRegex);
    if (rightParts == null)
        throw new Error('Error parsing the right section: ' + rightSection);
    const [, functionSection, definitionSection] = rightParts;
    const functions = functionSection.split(' '); // todo remove ( and )
    const definitions = definitionSection.split('/');

    return {
        definitions,
        functions,
        readings,
        writings,
    };
}
