# jadb

This projects aims to parse a file containing japanese words. It can be used to fill a database.

**Note**: As for now it will only print each word it can read.

## Config

Create a `private.json` and fill it in:
```json
{
  "dataBase": {
    "user": "USER",
    "password": "A_SECRET_TO_EVERYBODY",
    "host": "URL_TO_HOST",
    "name": "DATABASE_NAME"
  },
  "source": "PATH/TO/SOURCE.TXT"
}
```
Make sure to copy and paste or you won't be able to run this.

## Install

This project is tested for Node 14.17.3.

Clone this and run:
```shell
npm install
npm run start
```
