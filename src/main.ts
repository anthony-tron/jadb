import {createReadStream} from 'fs';
import {createInterface} from 'readline';
import {resolve} from 'path';
import {homedir} from 'os';
import {Settings} from './settings';
import {parseLine} from './parse';

const config = Settings.load('./private.json');
console.log('Reading from ', config.source);
const file = createReadStream(
    resolve(config.source.toString().replace(/^~/, homedir)),
    {
        encoding: 'utf-8'
    }

);
const reader = createInterface({
    input: file,
    crlfDelay: Infinity,
});


let i = 0;
reader.on('line', line => {
    if (i != 0) {
        console.log(parseLine(line));
    }
    ++i;
});
