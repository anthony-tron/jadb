export type Word = {
    definitions: string[],
    functions: string[],
    readings: string[],
    writings: string[],
};
