import {PathLike, readFileSync} from 'fs';

export namespace Settings {
    export type DataBaseConfig = {
        user: string,
        password: string,
        host: string,
        name: string,
    }

    export type Config = {
        dataBase: DataBaseConfig,
        source: PathLike,
    }

    export function load(path: PathLike): Config {
        const object = JSON.parse(readFileSync(path).toString());

        if (object.dataBase
            && object.dataBase.user
            && object.dataBase.password
            && object.dataBase.host
            && object.source)
            return object;

        throw new Error('Configuration file has errors');
    }
}
